## Multiblockchain network based on Hyperledger oficial tutorial Build Your First Network (BYFN):

The directions for using this are documented in the Hyperledger Fabric
["Build Your First Network"](http://hyperledger-fabric.readthedocs.io/en/latest/build_network.html) tutorial.

** Performance test-cases: (https://bitbucket.org/uhospital/multiblockchain/src/master/test/sample.js) **

