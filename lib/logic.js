/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
/**
 * Write your transction processor functions here
 */

/**
 * Sample transaction
 * @param {br.unisinos.uhospital.ehr.grantAccess} giveAccessToEHR -- give EHR access to doctor
 * @transaction
 */

 async function grantAccessToDoctor(giveAccessToEHR){
   if (giveAccessToEHR.medicalRecord.authorizedDoctors) {
        giveAccessToEHR.medicalRecord.authorizedDoctors.push(giveAccessToEHR.authorisedToModify);
    } else {
        giveAccessToEHR.medicalRecord.authorizedDoctors = [giveAccessToEHR.authorisedToModify];
    }
   
    let assetRegistry = await getAssetRegistry('br.unisinos.uhospital.ehr.MedicalRecord');
    await assetRegistry.update(giveAccessToEHR.medicalRecord);
   
   
   if (giveAccessToEHR.authorisedToModify.myPatients) {
        giveAccessToEHR.authorisedToModify.myPatients.push(giveAccessToEHR.medicalRecord.owner);
    } else {
        giveAccessToEHR.authorisedToModify.myPatients = [giveAccessToEHR.medicalRecord.owner];
    }
    
    let doctorRegistry = await getParticipantRegistry('br.unisinos.uhospital.ehr.Doctor');
    await doctorRegistry.update(giveAccessToEHR.authorisedToModify);

   
 }

 
 /**
 * Sample transaction
 * @param {br.unisinos.uhospital.ehr.revokeAccess} revokeAccessOfDoctor -- revoke EHR access to doctor
 * @transaction
 */
 async function revokeAccess(revokeAccessOfDoctor){
    var list = revokeAccessOfDoctor.medicalRecord.authorizedDoctors;
    var index = list.map(x => {
        return x.doctorId;
      }).indexOf(revokeAccessOfDoctor.revokeThisDoctor.doctorId);
      
      list.splice(index, 1);
    let assetRegistry = await getAssetRegistry('br.unisinos.uhospital.ehr.MedicalRecord');
    await assetRegistry.update(revokeAccessOfDoctor.medicalRecord);

    var patientList = revokeAccessOfDoctor.revokeThisDoctor.myPatients;
    var index = patientList.map(patient => {
        return patient;
      }).indexOf(revokeAccessOfDoctor.revokeThisDoctor.myPatients.patient);
      
      patientList.splice(index, 1);
    let doctorRegistry = await getParticipantRegistry('br.unisinos.uhospital.ehr.Doctor');
    await doctorRegistry.update(revokeAccessOfDoctor.revokeThisDoctor);
 }

 


/**
 * Create record Transaction
 * @param {br.unisinos.uhospital.ehr.createMedicalRecord} recordData
 * @transaction
 */
function createMedicalRecord(recordData) {
  return getParticipantRegistry('br.unisinos.uhospital.ehr.Patient')
      .then(function(patientRegistry){
    		return patientRegistry.exists(recordData.owner.getIdentifier())
      }).then(function(exists) {
       	  if(!exists) {
         	throw Error('Invalid cartaoSUS')
       	  } else {
            return getAssetRegistry('br.unisinos.uhospital.ehr.MedicalRecord')
            	.then(function(medicalRecordRegistry){
                    var  factory = getFactory();
                    var  NS =  'br.unisinos.uhospital.ehr';
                    var  recordId = generateRecordId(recordData.owner.cartaoSUS);
                    var  medicalRecord = factory.newResource(NS,'MedicalRecord',recordId);
              		medicalRecord.format = recordData.format;
              		medicalRecord.description = recordData.description;
                    medicalRecord.medicalHistory = recordData.medicalHistory;
                    medicalRecord.allergies = recordData.allergies;
                    medicalRecord.currentMedication = recordData.currentMedication;
                    medicalRecord.owner = recordData.owner;

                    return medicalRecordRegistry.add(medicalRecord);
              });
      }
      })
}


/****
* Creates the medical record 
*/
function generateRecordId(email){
  var number = Math.random();
  var id = email+number;
  return id;
}

/**
 * Handle a transaction that returns a string.
 * @param {br.unisinos.uhospital.ehr.getUserType} data The transaction.
 * @transaction
 */
async function getUserType(data) {
  return getParticipantRegistry('br.unisinos.uhospital.ehr.Doctor')
  .then(function (participantRegistry) {
    return participantRegistry.get(data.email);
  })
  .then(function (doctor) {
    if(doctor){
      return "Doctor";
    }
  })
  .catch(function (error) {
    return getParticipantRegistry('br.unisinos.uhospital.ehr.Patient')
    .then(function (patientRegistry) {
      return patientRegistry.get(data.email);
    })
    .then(function (patient) {
      if(patient){
        return "Patient";
      }
    })
    .catch(function (error) {
      return "no data";
    });
  });
  
}